var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var maxSize = 0.99
var initPos = []
var direction = []
var steps = 512
var translateX = []
var translateY = []

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  for (var i = 0; i < steps; i++) {
    initPos.push([-(1 - maxSize) + Math.random() * (1 - maxSize) * 2, -(1 - maxSize) + Math.random() * (1 - maxSize) * 2])
  }

  for (var i = 0; i < steps; i++) {
    if (initPos[i][0] > 0) {
      direction.push('+')
    } else {
      direction.push('-')
    }
    if (initPos[i][1] > 0) {
      direction.push('+')
    } else {
      direction.push('-')
    }
  }

  for (var i = 0; i < steps; i++) {
    var tempTranslateX = 0
    for (var j = 0; j < i + 1; j++) {
      tempTranslateX += initPos[i][0] * pow(maxSize, i)
    }
    translateX.push(tempTranslateX)
    tempTranslateX = 0

    var tempTranslateY = 0
    for (var j = 0; j < i + 1; j++) {
      tempTranslateY += initPos[i][1] * pow(maxSize, i)
    }
    translateX.push(tempTranslateY)
    tempTranslateY = 0
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < steps; i++) {
    fill(255 * ((i + 1) % 2))
    noStroke()
    push()
    translate(windowWidth * 0.5 + 0.5 * boardSize * translateX[i], windowHeight * 0.5 + 0.5 * boardSize * translateY[i])
    rect(0, 0, boardSize * pow(maxSize, (i + 1)), boardSize * pow(maxSize, (i + 1)))
    pop()
  }

  // masking
  fill(colors.light)
  noStroke()
  rect(windowWidth * 0.5, (windowHeight - boardSize) * 0.25, windowWidth, (windowHeight - boardSize) * 0.5 + 2)
  rect(windowWidth * 0.5, windowHeight * 0.75 + boardSize * 0.25, windowWidth, (windowHeight - boardSize) * 0.5 + 2)
  rect((windowWidth - boardSize) * 0.25, windowHeight * 0.5, (windowWidth - boardSize) * 0.5, windowHeight)
  rect(windowWidth * 0.75 + boardSize * 0.25, windowHeight * 0.5, (windowWidth - boardSize) * 0.5, windowHeight)

  for (var i = 0; i < steps; i++) {
    if (direction[2 * i] === '+') {
      initPos[i][0] += deltaTime * 0.000001
      if (initPos[i][0] > (1 - maxSize)) {
        direction[2 * i] = '-'
      }
    } else {
      initPos[i][0] -= deltaTime * 0.000001
      if (initPos[i][0] < -(1 - maxSize)) {
        direction[2 * i] = '+'
      }
    }

    if (direction[2 * i + 1] === '+') {
      initPos[i][1] += deltaTime * 0.000001
      if (initPos[i][1] > (1 - maxSize)) {
        direction[2 * i + 1] = '-'
      }
    } else {
      initPos[i][1] -= deltaTime * 0.000001
      if (initPos[i][1] < -(1 - maxSize)) {
        direction[2 * i + 1] = '+'
      }
    }
  }

  var tempTranslateX = 0
  var tempTranslateY = 0
  for (var i = 0; i < steps; i++) {
    for (var j = 0; j < i + 1; j++) {
      tempTranslateX += initPos[i][0] * pow(maxSize, i)
    }
    translateX[i] = tempTranslateX
    tempTranslateX = 0

    for (var j = 0; j < i + 1; j++) {
      tempTranslateY += initPos[i][1] * pow(maxSize, i)
    }
    translateY[i] = tempTranslateY
    tempTranslateY = 0
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
